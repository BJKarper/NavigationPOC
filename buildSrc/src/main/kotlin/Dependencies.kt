//package co.skillcheck.android.valet.bld

object Kotlin {
    private const val VERSION = "1.5.31"
    const val STD_LIB = "org.jetbrains.kotlin:kotlin-stdlib:$VERSION"

    private const val COROUTINES_VERSION = "1.5.0"
    const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${COROUTINES_VERSION}"
}

object AndroidX {
    private const val CORE_VERSION = "1.6.0"
    private const val VERSION = "1.4.0-alpha01"
    const val CORE = "androidx.core:core-ktx:$CORE_VERSION"
    const val APPCOMPAT = "androidx.appcompat:appcompat:$VERSION"
    const val FRAGMENT = "androidx.fragment:fragment-ktx:$VERSION"
}

object AndroidXNavigation {
    private const val VERSION = "2.4.0-beta01"
    private const val CURRENT_VERSION = "2.2.0"
    const val NAVIGATION = "androidx.navigation:navigation-ui-ktx:$CURRENT_VERSION"
    const val NAVIGATION_FRAGMENT = "androidx.navigation:navigation-fragment-ktx:$CURRENT_VERSION"
}

object AndroidXLifecycle {
    private const val VERSION = "2.3.1"
    const val COMMON = "androidx.lifecycle:lifecycle-common:$VERSION"
    const val RUNTIME = "androidx.lifecycle:lifecycle-runtime:$VERSION"
    const val EXTENSIONS = "android.arch.lifecycle:extensions:$VERSION"
    const val LIVEDATA = "androidx.lifecycle:lifecycle-livedata-ktx:$VERSION"
}

object Compose {
    private const val VERSION = "1.0.4"
    const val COMPILER = VERSION //"androidx.compose.compiler:compiler:$VERSION"
    const val UI = "androidx.compose.ui:ui:$VERSION"
    const val TOOLING = "androidx.compose.ui:ui-tooling:$VERSION"
    const val FOUNDATION = "androidx.compose.foundation:foundation:$VERSION"
    const val FOUNDATION_LAYOUT = "androidx.compose.foundation:foundation-layout:$VERSION"
    const val MATERIAL = "androidx.compose.material:material:$VERSION"
    const val MATERIAL_ICONS = "androidx.compose.material:material-icons-core:$VERSION"
    const val MATERIAL_ICONS_EXTENDED = "androidx.compose.material:material-icons-extended:$VERSION"
    const val VIEW_BINDING = "androidx.compose.ui:ui-viewbinding:$VERSION"
    const val LIVEDATA = "androidx.compose.runtime:runtime-livedata:$VERSION"
}

object ComposeNavigation {
    private const val VERSION = "2.4.0-alpha10"
    const val NAVIGATION = "androidx.navigation:navigation-compose:$VERSION"
}

object ComposeActivity {
    private const val VERSION = "1.3.1"
    const val ACTIVITY = "androidx.activity:activity-compose:$VERSION"
}

object Dagger {
    private const val VERSION = "2.37"
    const val HILT = "com.google.dagger:hilt-android:$VERSION"
    const val HILT_VIEWMODEL = "androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03"
    const val HILT_COMPILER = "com.google.dagger:hilt-compiler:$VERSION"
}

object Accompanist {
    private const val VERSION = "0.21.0-beta"
    const val THEME_ADAPTER = "com.google.accompanist:accompanist-appcompat-theme:$VERSION"
    const val SYSTEM_UI_CONTROLLER = "com.google.accompanist:accompanist-systemuicontroller:$VERSION"
    const val SWIPE_REFRESH = "com.google.accompanist:accompanist-swiperefresh:$VERSION"
}

object Retrofit {
    private const val VERSION = "2.9.0"
    const val RETROFIT = "com.squareup.retrofit2:retrofit:$VERSION"
    const val JACKSON = "com.squareup.retrofit2:converter-jackson:$VERSION"
}

object OkHttp {
    private const val VERSION = "4.9.1"
    const val OKHTTP = "com.squareup.okhttp3:okhttp:$VERSION"
    const val LOGGING_INTERCEPTOR = "com.squareup.okhttp3:logging-interceptor:$VERSION"
    const val MOCK_SERVER = "com.squareup.okhttp3:mockwebserver:$VERSION"
}

object JUnit {
    private const val JUNIT_VERSION = "4.12"
    const val JUNIT = "junit:junit:${JUNIT_VERSION}"

    private const val ANDROIDX_JUNIT_VERSION = "1.1.0"
    private const val ANDROIDX_JUNIT_RUNNER_VERSION = "1.2.0"
    const val ANDROIDX_JUNIT = "androidx.test.ext:${ANDROIDX_JUNIT_VERSION}"
    const val ANDROIDX_TEST_RUNNER = "androidx.test:runner:${ANDROIDX_JUNIT_RUNNER_VERSION}"

    private const val COMPOSE_VERSION = "1.0.0-beta08"
    const val COMPOSE_JUNIT = "androidx.compose.ui:ui-test-junit4${COMPOSE_VERSION}"
}

object AndroidTest {
    private const val VERSION = "3.2.0"
    const val ESPRESSO_CORE = "androidx.test.espresso:espresso-core:$VERSION"

    private const val COMPOSE_VERSION = "1.0.0-beta08"
    const val COMPOSE_TESTING = "androidx.compose.ui:ui-test-junit4${COMPOSE_VERSION}"
}

object AndroidVersions {
    const val MINIMUM_SDK = 21
    const val TARGET_SDK = 31
    const val COMPILE_SDK = 31
}