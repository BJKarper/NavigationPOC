/*
 * Copyright (C) 2017 The Dagger Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.navigationpoc.di

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.HasAndroidInjector
import dagger.internal.Beta
import dagger.internal.Preconditions


/** Injects core Android types.  */
@Beta
object AndroidXInjection {
    private const val TAG = "dagger.android"

    /**
     * Injects `activity` if an associated [AndroidInjector] implementation can be found,
     * otherwise throws an [IllegalArgumentException].
     *
     * @throws RuntimeException if the [Application] doesn't implement [     ].
     */
    fun inject(activity: AppCompatActivity) {
        Preconditions.checkNotNull(activity, "activity")
        val application = activity.application
        if (application !is HasAndroidInjector) {
            throw RuntimeException(
                String.format(
                    "%s does not implement %s",
                    application.javaClass.canonicalName,
                    HasAndroidInjector::class.java.canonicalName
                )
            )
        }
        inject(activity, application as HasAndroidInjector)
    }

    /**
     * Injects `fragment` if an associated [AndroidInjector] implementation can be found,
     * otherwise throws an [IllegalArgumentException].
     *
     *
     * Uses the following algorithm to find the appropriate `AndroidInjector<Fragment>` to
     * use to inject `fragment`:
     *
     *
     *  1. Walks the parent-fragment hierarchy to find the a fragment that implements [       ], and if none do
     *  1. Uses the `fragment`'s [activity][Fragment.getActivity] if it implements
     * [HasAndroidInjector], and if not
     *  1. Uses the [android.app.Application] if it implements [HasAndroidInjector].
     *
     *
     * If none of them implement [HasAndroidInjector], a [IllegalArgumentException] is
     * thrown.
     *
     * @throws IllegalArgumentException if no parent fragment, activity, or application implements
     * [HasAndroidInjector].
     */
    fun inject(fragment: Fragment) {
        Preconditions.checkNotNull(fragment, "fragment")
        val hasAndroidInjector = findHasAndroidInjectorForFragment(fragment)
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(
                TAG, String.format(
                    "An injector for %s was found in %s",
                    fragment.javaClass.canonicalName,
                    hasAndroidInjector.javaClass.canonicalName
                )
            )
        }
        inject(fragment, hasAndroidInjector)
    }

    private fun findHasAndroidInjectorForFragment(fragment: Fragment?): HasAndroidInjector {
        var parentFragment = fragment
        while (parentFragment?.parentFragment.also { parentFragment = it } != null) {
            if (parentFragment is HasAndroidInjector) {
                return parentFragment as HasAndroidInjector
            }
        }
        val activity = fragment?.activity
        if (activity is HasAndroidInjector) {
            return activity
        }
        if (activity?.application is HasAndroidInjector) {
            return activity.application as HasAndroidInjector
        }
        throw IllegalArgumentException(
            String.format(
                "No injector was found for %s",
                fragment?.javaClass?.canonicalName
            )
        )
    }

    private fun inject(target: Any, hasAndroidInjector: HasAndroidInjector) {
        val androidInjector = hasAndroidInjector.androidInjector()
        Preconditions.checkNotNull(
            androidInjector, "%s.androidInjector() returned null", hasAndroidInjector.javaClass
        )
        androidInjector.inject(target)
    }
}