package com.example.navigationpoc.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.navigationpoc.MainActivity
import com.example.navigationpoc.ui.main.MainFragment
import com.example.navigationpoc.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @ContributesAndroidInjector
    fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    fun contributesMainFragment(): MainFragment

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindsMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    fun bindsViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}