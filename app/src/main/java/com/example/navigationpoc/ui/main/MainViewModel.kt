package com.example.navigationpoc.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor() : ViewModel() {

    private val _listLiveData = MutableLiveData<List<Int>>()
    val listLiveData: LiveData<List<Int>> = _listLiveData

    private val mainList:MutableList<Int> = mutableListOf()

    init {
        repeat(30) {
            mainList.add(it)
        }
        _listLiveData.value = mainList
    }
}