package com.example.navigationpoc.ui.main

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.BottomSheetScaffold
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.navigationpoc.POCApplication
import com.example.navigationpoc.R
import com.example.navigationpoc.di.AndroidXInjection
import com.example.navigationpoc.ui.account.DetailActivity
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    //var viewModel: MainViewModel? = null
    private val viewModel: MainViewModel by viewModels { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidXInjection.inject(this)
        //viewModel = ViewModelProviders.of(this, viewModelFactory)
        //(activity?.application as POCApplication).dispatchingAndroidInjector.inject(this)
    }

    @OptIn(ExperimentalMaterialApi::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {

                val navController: NavController = findNavController()
                val list = viewModel.listLiveData.observeAsState()
                BottomSheetScaffold(sheetPeekHeight = 0.dp, sheetContent = {
                    Column {}
                }) {

                    SwipeRefresh(
                        state = rememberSwipeRefreshState(isRefreshing = false),
                        onRefresh = { /*TODO*/ }) {

                        Column(
                            Modifier
                                .verticalScroll(rememberScrollState())
                                .fillMaxWidth()
                                .fillMaxHeight()
                        ) {
                            list.value?.forEach {
                                if (it == 29) {
                                    RowItem(number = it) {
                                        val intent = Intent(context, DetailActivity::class.java)
                                        startActivity(intent)
                                    }
                                } else {
                                    RowItem(number = it) {
                                        navController.navigate(R.id.action_mainFragment_to_dummyFragment)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @Composable
    private fun RowItem(number: Int, onClick: () -> Unit) {
        Row(
            Modifier
                .height(48.dp)
                .fillMaxWidth()
                .clickable {
                    onClick.invoke()
                }
        ) {
            Text(
                text = "Item: ${number + 1}",
                modifier = Modifier
                    .fillMaxHeight()
                    .fillMaxWidth()
                    .wrapContentSize(align = Alignment.CenterStart)
                    .padding(start = 8.dp)
            )
        }
    }
}